use std::env;
use siteicon::get_favicon_url;

#[tokio::main]
async fn main() {
    let args: Vec<String> = env::args().collect();
    let domain = args[1].clone();

    let url = get_favicon_url(domain).await;

    println!("Found URL {:?}", url);

}
