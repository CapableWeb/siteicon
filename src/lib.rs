use isahc::{config::RedirectPolicy, prelude::*, HttpClient, http::StatusCode};
use std::time::Duration;
use scraper::Html;
use scraper::Selector;

// Enable logging
static DEBUG: bool = false;

pub async fn get_favicon_url(domain: String) -> Option<String> {
    let url = format!("https://{}/favicon.ico", domain);

    let client = HttpClient::builder()
        .timeout(Duration::from_secs(5))
        .default_header("user-agent", "Mozilla/5.0 (X11; Linux x86_64; rv:97.0) Gecko/20100101 Firefox/97.0")
        .redirect_policy(RedirectPolicy::Limit(10))
        .build()
        .unwrap();

    // println!("{:?}", url);

    let response = client.get_async(&url).await;
    let found_favicon = match response {
        Ok(res) => {
            if DEBUG {
                println!("{:?}",  res.status());
            }
            let status = res.status();
            match status {
                StatusCode::OK => {
                    match res.body().len() {
                        Some(l) => {
                            if l > 10 {
                                true
                            } else {
                                false
                            }
                        },
                        None => false
                    }
                },
                StatusCode::MOVED_PERMANENTLY => true,
                StatusCode::FOUND => true,
                _ => false
            }
        },
        Err(_) => false,
    };
    if found_favicon {
        return Some(url)
    } else {
        // println!("Need to find icon somewhere else");
        // TODO not always https, should try http too
        let url = format!("https://{}", domain);
        let mut response = match client.get_async(&url).await {
            Ok(res) => res,
            Err(_) => {
                return None
            }
        };

        let body = response.text().await.unwrap();
        let document = Html::parse_document(&body);
        let selector = Selector::parse("link[rel~=icon]").unwrap();

        let mut href = None;
        let mut selected_elements = document.select(&selector);

        while href == None {
            let selected_element = selected_elements.next();

            if selected_element == None {
                // Here we could try to just get the first image we can find, maybe?
                // or any image element where width and height are the same, might work
                // too
                return None
            }

            let first_element = selected_element.unwrap();

            let new_href = first_element.value().attr("href");
            href = match new_href {
                Some(h) => match h {
                    "" => None,
                    _ => {
                        Some(h)
                        // if h.contains(".svg") {
                        //     Some(h)
                        // } else {
                        //     None
                        // }
                    }
                },
                None => None
            };
        }
        let href: String = href.unwrap().to_string();

        if DEBUG {
            println!("Ended up with href => {:?} ", href);
        }

        // We need to check what kind of URL href is, could be relative, absolute, full
        let mut it = href.chars();
        let ch1 = it.next().unwrap();
        let ch2 = it.next().unwrap();

        if DEBUG {
            println!("({}, {})", ch1, ch2);
        }

        let full_url = match ch1 {
            // Domain + Absolute
            '/' => {
                match ch2 {
                    '/' => format!("https:{}", href),
                    _ => format!("{}{}", url, href)
                }
            },
            // Full
            'h' => format!("{}", href),
            // Relative
            '.' => format!("{}{}", url, href),
            _ => format!("{}/{}", url, href)
        };

        // let full_url = format!("{}{}", url, href);
        if DEBUG {
            println!("{:#?}", full_url);
        }
        // for element in document.select(&selector) {
            // assert_eq!("li", element.value().name());
        // }
        Some(full_url)
    }
}
